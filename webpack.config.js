const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const SRC_DIR = path.resolve(__dirname, 'app');
const ENV = process.env.npm_lifecycle_event;
const host = '192.168.178.21';
const port = 3000;

const plugins = [
  new HtmlWebpackPlugin({ template: './app/index.html' }),
  new ExtractTextPlugin('css/main.css', { allChunks: true }),
  new CopyWebpackPlugin([ { from: 'app/media', to: 'media' } ]),
];

if (ENV === 'prod') {
  plugins.push(
    new CleanWebpackPlugin(['build'], {
        root: path.join(__dirname), verbose: true, dry: false }),
    new webpack.optimize.UglifyJsPlugin({
        beautyfy: false, sourceMap: false, compress: { warnings: false } }),
    new webpack.DefinePlugin({ 'process.env': { 'NODE_ENV': '"production"' } } )
  );
} else {
  plugins.push(new webpack.HotModuleReplacementPlugin());
}

const AUTOPREFIXER_BROWSERS = [
  'Android 2.3',
  'Android >= 4',
  'Chrome >= 35',
  'Firefox >= 31',
  'Explorer >= 9',
  'iOS >= 7',
  'Opera >= 12',
  'Safari >= 7.1',
];

const config = {
  context: __dirname,
  entry: [
    SRC_DIR + '/js/index.js',
    SRC_DIR + '/style/app.scss',
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'js/main.js'
  },
  module: {
    rules: [
      {
        test: /\.js|.jsx$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['react', 'es2015', 'stage-0'],
            plugins: ['emotion'],
          }
        }]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: 'css-loader',
              options: {
              url: false,
              minimize: ENV === 'prod' ? true : false,
              sourceMap: true
            }
          }, 
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')({ browsers: AUTOPREFIXER_BROWSERS })
              ]
            }
          },
          { loader: 'sass-loader' }]
        })
      }
    ]
  },
  plugins: plugins,
  devServer: {
    contentBase: './build',
    hot: true,
    port,
    public: host + ':' + port,
    stats: 'minimal',
    open: true
  }
};

module.exports = config;
