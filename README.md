Pokemon React Application
==========================

## Installation:
1) Install node.js

2) Open Terminal and "cd path_to_project_root"
```
3) npm install
```
4) Change IP address in webpack.config.js to make it accessible for other devices on you wi-fi network

## Commands:
```
npm start - run development environment, it starts webpack dev server

npm run prod - create fully minified production-ready files in build folder.
```

## How to deploy to server:
```
1) npm run prod
```
2) Copy files from build folder to your server.

## Included:

- **React**
- **React-Dom**
- **Redux**
- **React router**
- **Bootstrap** 
- **SASS** 
- **Babel**
- **Webpack**
- **Webpack dev server**


**Live Reload** included - webpack watch all your files and reload on every save.
