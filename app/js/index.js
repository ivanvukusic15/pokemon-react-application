import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';

import App from './App';
import Home from './pages/Home';
import Favorites from './pages/Favorites';
import Contact from './pages/Contact';
import NotFound from './pages/404page';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

ReactDOM.render(
  <Provider store = { store }>
    <Router history = { browserHistory }>
      <Route path = "/" component = { App }>
        <IndexRoute component = { Home } />
        <Route path = "/list" component = { Home } />
        <Route path = "/favorites" component = { Favorites } />
        <Route path = "/contact" component = { Contact } />
        <Route path = "/*" component = { NotFound } />
      </Route>
    </Router>
  </Provider>
, document.getElementById('app'));
