import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';

class Header extends Component {
  constructor (props) {
    super(props);
    this.state = {
      opened: false
    };
  }

  render () {
    const currentRoute = browserHistory.getCurrentLocation();
    const ROUTES = [
      { name: 'HOME', path: '/' },
      { name: 'LIST', path: '/list' },
      { name: 'FAVORITES', path: '/favorites' },
      { name: 'CONTACT', path: '/contact' },
    ];
    return (
      <header>
        <nav>
          <ul className = "desktop-nav">
            {
              ROUTES.map( (route, i) => {
                return (
                  <li 
                    key = { i }
                    className = { currentRoute.pathname === route.path ? 'selected' : '' } >
                    <Link href = { route.path }>{ route.name }</Link>
                  </li>
                );
              })
            }
          </ul> 
          <div className = { this.state.opened ? 'mobile-nav' : 'mobile-nav closed' }>
            <div className = "nav-label-button">
              <span className = "label">Select Page</span>
              <div 
                onClick = { () => this.setState({ opened: !this.state.opened }) }
                className = "button" >
                <div className = "line" />
                <div className = "line" />
                <div className = "line" />
              </div>
            </div>
            <ul className = "mobile-nav-items">
              {
                ROUTES.map( (route, i) => {
                  return (
                    <li 
                      key = { i }
                      className = { currentRoute.pathname === route.path ? 'selected' : '' } >
                      <Link href = { route.path }>{ route.name }</Link>
                    </li>
                  );
                })
              }
            </ul> 
          </div>
        </nav>
      </header>
    );
  }
};

export default Header;
