import React from 'react';
import PropTypes from 'prop-types';

export const Star = ({ star, updateFavorites }) => {
  return (
    <div 
      className = "star-container"
      onClick = { () => updateFavorites() }>
      {
        star
        ?
          <span className = "star star-favorite">&#9733;</span>
        :
          <span className = "star">&#9734;</span>
      }
    </div>
  );
};

Star.propTypes = {
  star: PropTypes.object,
  updateFavorites: PropTypes.func,
};
