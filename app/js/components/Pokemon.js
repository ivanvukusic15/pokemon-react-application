import React from 'react';
import PropTypes from 'prop-types';
import { Star } from './';

export const Pokemon = ({ id, pokemon, favorites, selectPokemon, updateFavorites }) => {
  
  const image = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id + '.png';
  return (
    <div className = "pokemon">
      <div 
        className = "pokemon-img-name"
        onClick = { () => selectPokemon(pokemon.url, pokemon.name, image) }>
        <img 
          onError = { addDefaultSrc } 
          className = "image" 
          src = { image } 
          alt = { pokemon.name }/>

        <span className = "name">{ pokemon.name }</span>
      </div>
      <Star
        pokemon = { pokemon }
        star = { favorites[id] }
        updateFavorites = { () => updateFavorites(id, pokemon) } />
    </div>
  );
};

Pokemon.propTypes = {
  id: PropTypes.number,
  pokemon: PropTypes.object,
  selectPokemon: PropTypes.func,
  favorites: PropTypes.object,
  updateFavorites: PropTypes.func,
};

const addDefaultSrc = (ev) => {
  ev.target.src = './media/images/no_image.png';
  ev.target.className = 'no-image';
};
