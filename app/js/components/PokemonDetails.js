import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { HashLoader } from 'react-spinners';
import { Star } from './';
import { connect } from 'react-redux';
import { deselectPokemon, updateFavorites } from '../actions';

class PokemonDetails extends Component {

  static propTypes = {
    favorites: PropTypes.object,
    pokemon: PropTypes.object,
    deselectPokemon: PropTypes.func,
    updateFavorites: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  renderAbilities (abilities) {
    return (
      <div className = "list">
        <h3>ABILITIES</h3>
        <ul>
          {
            abilities.map( (ability, i) => {
              return <li key = { i }>{ ability['ability'].name }</li>;
            })
          }
        </ul>
      </div>
    );
  }

  renderStats (stats) {
    return (
      <div className = "list">
        <h3>STATS</h3>
        {
          stats.map( (stat, i) => {
            return (
              <div key = { i } className = "row">
                <label>{ stat['stat'].name[0].toUpperCase() + stat['stat'].name.slice(1) }:</label><span>{ stat.base_stat }</span>
              </div>
            );
          })
        }
      </div>
    );
  }

  renderMoves (moves) {
    return (
      <div className = "list list-moves">
        <h3>MOVES</h3>
        <ul>
          {
            moves.map( (move, i) => {
              return <li key = { i }>{ move['move'].name }</li>;
            })
          }
        </ul>
      </div>
    );
  }

  render () {
    const { pokemon, favorites } = this.props;
    if (pokemon) {
      const { id, weight, height, image, name, sprites, types, abilities, stats, moves } = pokemon;
      const baseExperience = pokemon.base_experience;
      let type = '';
      let img = image;
      if (sprites && sprites.front_default) {
        img = sprites.front_default;
      }
      if (types && types.length > 0) {
        types.map( (tip) => {
          if (type.length === 0) {
            type = tip['type'].name;
          } else {
            type += ' / ' + tip['type'].name;
          }
        });
      }
      return (
        <div 
          className = "modal">
          <div className = "hide-container"
            onClick = { () => this.props.deselectPokemon() } />
          <div className = { pokemon.loading ? 'inner center' : 'inner' }>
            {
              pokemon.loading
              ?
                <HashLoader
                  color = { '#4E4321' } 
                  loading = { pokemon.loading } />
              :
                <div className = "pokemon-details">
                  <h2>{ name }</h2>
                  <div>
                    <div className = "pokemon-image-text">
                      <img 
                        onError = { addDefaultSrc } 
                        className = "image" 
                        src = { img } 
                        alt = { name }/>
                      <div className = "column">
                        <div className = "row">
                          <label>ID:</label><span>{ id }</span>
                        </div>
                        <div className = "row">
                          <label>Type:</label><span>{ type }</span>
                        </div>
                        <div className = "row">
                          <label>Weight:</label><span>{ weight }</span>
                        </div>
                        <div className = "row">
                          <label>Height:</label><span>{ height }</span>
                        </div>
                        <div className = "row">
                          <label>Experience:</label><span>{ baseExperience }</span>
                        </div>
                      </div>
                    </div>
                    {
                      abilities && abilities.length > 0
                      ?
                        this.renderAbilities(abilities)
                      : 
                        null
                    }
                    {
                      stats && stats.length > 0
                      ?
                        this.renderStats(stats)
                      : 
                        null
                    }
                    {
                      moves && moves.length > 0
                      ?
                        this.renderMoves(moves)
                      : 
                        null
                    }
                  </div>
                </div>
            }
            {
              !pokemon.loading
              ?
                <Star
                  pokemon = { pokemon }
                  star = { favorites[id] }
                  updateFavorites = { () => {
                    const poke = {
                      url: 'https://pokeapi.co/api/v2/pokemon/' + id,
                      name: pokemon.name
                    };
                    this.props.updateFavorites({ id, pokemon: poke, favorites });
                  } } />
              :
                null
            }
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
};

const addDefaultSrc = (ev) => {
  ev.target.src = './media/images/no_image.png';
  ev.target.className = 'no-image';
};

const mapStateToProps = ({ pokemons }) => {
  const pokemon = pokemons.selected;
  return { pokemon, favorites: pokemons.favorites };
};

export default connect(mapStateToProps, { deselectPokemon, updateFavorites })(PokemonDetails);
