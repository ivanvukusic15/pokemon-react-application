import React from 'react';

export const Footer = () => {
  return (
    <footer>
      <p className = "copyright">© { new Date().getFullYear() } ALL RIGHT RESERVED {}
        <a href ="mailto: ivanvukusic15gmail.com">ivukusic</a>.</p>
    </footer>
  );
};
