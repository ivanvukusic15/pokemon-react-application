import React, { Component } from 'react';
import { Footer } from './components/';
import Header from './components/Header';
import PokemonDetails from './components/PokemonDetails';
import PropTypes from 'prop-types';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Header />
        { this.props.children }
        <Footer />
        <PokemonDetails />
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object,
  pokemon: PropTypes.object,
  deselectPokemon: PropTypes.func
};

export default App;
