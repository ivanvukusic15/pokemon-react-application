import {
  POKEMON_LIST_URL
} from '../constants/url';
import {
  POKEMON_LIST,
  POKEMON_LIST_LOADING,
  POKEMON_SELECT,
  POKEMON_DESELECT,
  POKEMON_UPDATE_FAVORITES,
} from '../constants/types';

export const getLocalStorage = () => {
  let payload = localStorage.getItem('favorite_pokemons');
  if (payload) {
    payload = JSON.parse(payload);
  } else {
    payload = {};
  }
  return { 
    type: POKEMON_UPDATE_FAVORITES, 
    payload
  };
};

export const getPokemonList = ({ offset }) => {
  return (dispatch) => {
    successToReducer({ dispatch, type: POKEMON_LIST_LOADING, payload: true });
    let url = POKEMON_LIST_URL + '?limit=100';
    if (offset) {
      url += '&offset=' + offset;
    }
    fetch(url)
      .then( (response) => {
        return response.json();
      }).then((result)=> {
        successToReducer({ dispatch, type: POKEMON_LIST, payload: result });
      }).catch ( (error) => {
        return { error };
      });
    };
};

export const selectPokemon = ({ name, image, url }) => {
  return (dispatch) => {
    const pokemon = { name, image, loading: true };
    successToReducer({ dispatch, type: POKEMON_SELECT, payload: pokemon });
    fetch(url)
      .then( (response) => {
        return response.json();
      }).then((result)=> {
        successToReducer({ dispatch, type: POKEMON_SELECT, payload: result });
      }).catch ( (error) => {
        return { error };
      });
    };
};

export const deselectPokemon = () => {
  return (dispatch) => {
    successToReducer({ dispatch, type: POKEMON_DESELECT, payload: null });
  };
};

export const updateFavorites = ({ id, pokemon, favorites }) => {
  if (favorites[id]) {
    delete favorites[id];
  } else {
    favorites[id] = pokemon;
  }
  localStorage.setItem('favorite_pokemons', JSON.stringify(favorites));
  return { 
    type: POKEMON_UPDATE_FAVORITES, 
    payload: favorites
  };
};

const successToReducer = ({ dispatch, type, payload }) => {
  dispatch({ type, payload });
};
