import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Pokemon } from '../components';
import { HashLoader } from 'react-spinners';
import {
  getLocalStorage,
  getPokemonList,
  selectPokemon,
  updateFavorites,
} from '../actions';

class Home extends Component {

  static propTypes = {
    getLocalStorage: PropTypes.func,
    getPokemonList: PropTypes.func,
    selectPokemon: PropTypes.func,
    updateFavorites: PropTypes.func,
    pokemons: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      favorites: []
    };
  }
  
  componentWillMount () {
    this.props.getLocalStorage();
    this.props.getPokemonList({ offset: 0 });
    this.onScroll = window.addEventListener('scroll', () => this.handleScroll() );
  }

  componentWillUnmount() {
    this.onScroll.remove();
  }

  handleScroll () {
    const { count, pokemons, loading } = this.props.pokemons;
    if (!loading && pokemons.length < count) {
        this.loadMore(this.props.pokemons.pokemons.length);
    }
  }

  loadMore (count) {
    this.props.getPokemonList({ offset: count });
  }

  renderSearchBar () {
    return (
      <div className = "search-bar">
        <input 
          type = "text" 
          value = { this.state.search } 
          onChange = { (event) => this.setState({ search: event.target.value }) }
          placeholder = "&#128270; &nbsp; &nbsp; Enter pokemon name" />
      </div>
    );
  }

  render() {
    const { pokemons, loading, favorites } = this.props.pokemons;
    const { search } = this.state;
    return (
      <section 
        style = { { minHeight: (window.innerHeight - 200 - 400) } }
        className = "home-page">

        <h1>Pokemons</h1>

        { this.renderSearchBar() }

        {
          pokemons && pokemons.length > 0
          ?
            <div className = "pokemons-container">
              {
                pokemons.map( (pokemon, i) => {
                  let poke = null;
                  if ( (search && pokemon.name.toLowerCase().includes(search.toLowerCase()) ) || search.length === 0 ) {
                    poke = pokemon;
                  }
                  if (poke) {
                    const id = parseInt(poke.url.replace('https://pokeapi.co/api/v2/pokemon/', '').replace('/',''), 10);
                    return (
                      <Pokemon 
                        key = { i } 
                        id = { id }
                        pokemon = { pokemon }
                        selectPokemon = { (url, name, image) => this.props.selectPokemon({ url, name, image }) }
                        favorites = { favorites }
                        updateFavorites = { (id) => this.props.updateFavorites({ id, pokemon: poke, favorites }) } />
                    );
                  }
                })
              }
            </div>
          :
            null
        }
        <div className = "row center loader">
          <HashLoader
            color = { '#4E4321' } 
            loading = { loading } />
        </div>
      </section>
    );
  }
}

const mapStateToProps = ({ pokemons }) => {
  return { pokemons };
};

export default connect(mapStateToProps, {
  getLocalStorage,
  getPokemonList,
  selectPokemon,
  updateFavorites,
})(Home);
