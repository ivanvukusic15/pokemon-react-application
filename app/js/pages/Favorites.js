import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Pokemon } from '../components';
import {
  getLocalStorage,
  selectPokemon,
  updateFavorites,
} from '../actions';

class Favorites extends Component {

  static propTypes = {
    favorites: PropTypes.object,
    loading: PropTypes.bool,
    getLocalStorage: PropTypes.func,
    selectPokemon: PropTypes.func,
    updateFavorites: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
  }

  componentWillMount () {
    this.props.getLocalStorage();
  }

  render() {
    const { favorites } = this.props;
    const { search } =  this.state;
    let pokemons = [];
    if (favorites) {
      pokemons = _.values(favorites);
    }
    return (
      <section 
        className = "favorites-page"
        style = { { minHeight: (window.innerHeight - 200 - 400) } }>
        
        <h1>Favorite pokemons</h1>
        
        {
          pokemons && pokemons.length > 0
          ?
            <div className = "row wrap center">
              {
                pokemons.map( (pokemon, i) => {
                  let poke = null;
                  if ( (search && pokemon.name.toLowerCase().includes(search.toLowerCase()) ) || search.length === 0 ) {
                    poke = pokemon;
                  }
                  if (poke) {
                    const id = parseInt(poke.url.replace('https://pokeapi.co/api/v2/pokemon/', '').replace('/',''), 10);
                    return (
                      <Pokemon 
                        key = { i } 
                        id = { id }
                        pokemon = { pokemon }
                        selectPokemon = { (url, name, image) => this.props.selectPokemon({ url, name, image }) }
                        favorites = { favorites }
                        updateFavorites = { (id) => this.props.updateFavorites({ id, pokemon: poke, favorites }) } />
                    );
                  }
                })
              }
            </div>
          :
            null
        }
        {/* <div className = "row center loader">
          <HashLoader
            color = { '#4E4321' } 
            loading = { loading } />
        </div> */}

      </section>
    );
  }

}

const mapStateToProps = ({ pokemons }) => {
  return { favorites: pokemons.favorites, loading: pokemons.loading };
};

export default connect(mapStateToProps, {
  getLocalStorage,
  selectPokemon,
  updateFavorites,
})(Favorites);
