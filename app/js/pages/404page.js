import React, { Component } from 'react';
import { connect } from 'react-redux';

class NotFound extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section 
        className = "notfound-page"
        style = { { minHeight: (window.innerHeight - 200 - 400) } }>
        
        <h1>404 page</h1>

      </section>
    );
  }

}

export default connect(null)(NotFound);
