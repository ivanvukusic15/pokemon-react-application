import React, { Component } from 'react';
import { connect } from 'react-redux';

class Contact extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section 
        className = "contact-page"
        style = { { minHeight: (window.innerHeight - 200 - 400) } }>
        
        <h1>Contact page</h1>

      </section>
    );
  }

}

export default connect(null)(Contact);
