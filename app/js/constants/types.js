export const POKEMON_LIST_LOADING = 'pokemon_list_loading';
export const POKEMON_LIST = 'pokemon_list';
export const POKEMON_SELECT = 'pokemon_select';
export const POKEMON_DESELECT = 'pokemon_deselect';
export const POKEMON_UPDATE_FAVORITES = 'pokemon_update_favorites';
