import {
  POKEMON_LIST, 
  POKEMON_LIST_LOADING,
  POKEMON_SELECT,
  POKEMON_DESELECT,
  POKEMON_UPDATE_FAVORITES,
} from '../constants/types';

const INITIAL_STATE = {
  count: 0,
  next: '',
  previous: '',
  pokemons: [],
  selected: null,
  favorites: {},
  favoritesCount: 0,
};
  
export default function (state = INITIAL_STATE, action) {
  switch(action.type) {
    case POKEMON_LIST: 
      return { 
        ...state,
        count: action.payload.count, 
        next: action.payload.next, 
        previous: action.payload.previous,
        pokemons: [ ...state.pokemons, ...action.payload.results],
        loading: false,
      };
    case POKEMON_LIST_LOADING: 
      return { 
        ...state,
        loading: true,
      };
    case POKEMON_SELECT: 
      const selected = action.payload;
      if (selected.sprites && selected.sprites.front_default) {
        selected.image = selected.sprites.front_default;
      } else if (state.selected && state.selected.image) {
        selected.image = state.selected.image;
      }
      return { ...state, selected };
    case POKEMON_DESELECT: 
      return { 
        ...state,
        selected: null
      };
    case POKEMON_UPDATE_FAVORITES: 
      return { ...state, favorites: { ...action.payload } };
    default: 
      return { ...state };
  }
  return state;
}
