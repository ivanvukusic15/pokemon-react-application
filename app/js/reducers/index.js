import { combineReducers } from 'redux';
import authentification from './authentification';
import pokemons from './pokemons';

const rootReducer = combineReducers({
  authentification,
  pokemons
});

export default rootReducer;
